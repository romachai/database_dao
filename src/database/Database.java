/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Database {
     static String url = "jdbc:sqlite:./librarydb/library.db";
     static Connection conn = null;  
     
      public static Connection connect() {
          if(conn!=null) return conn;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect Database");
            return conn;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
 
         public static void close() {
        try {
            if(conn!=null){
                conn.close();
            System.out.println("Close Database");
            conn = null;
             }   
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
}
