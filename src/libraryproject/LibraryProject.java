/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Database;
import database.User;
import database.UserDao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;

/**
 *
 * @author informatics
 */
public class LibraryProject {
 static String url = "jdbc:sqlite:./librarydb/library.db";
 static Connection conn = null;  
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
     
        User newUser = new User();
        newUser.setUserId(-1);
        newUser.setLoginName("ABC");
        newUser.setPassword("password");
        newUser.setName("ABC");
        newUser.setSurname("DEF");
        newUser.setTypeId(1);
        UserDao.insert(newUser);
        
        ArrayList<User> list = UserDao.getUsers();
        for(User user:list){
            System.out.println(user);
        }
    }


    
    
}
